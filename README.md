# 08-solition.package-registry


si CI
```bash
curl --header "JOB-TOKEN:$CI_JOB_TOKEN" \
     --upload-file target/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}-SNAPSHOT.jar \
     "$RELEASE_PATH"
```

https://docs.gitlab.com/ee/user/packages/generic_packages/
```bash
PROJECT_ID="31637767"
PACKAGE_NAME="my_package"
PACKAGE_VERSION="0.0.1"
FILE_NAME="hello.txt"
echo "https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${PACKAGE_VERSION}/${FILE_NAME}?status=default"
curl -X PUT --header "PRIVATE-TOKEN:${GITLAB_TOKEN_ADMIN}" \
     --upload-file ./hello.txt \
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${PACKAGE_VERSION}/${FILE_NAME}?status=default"

PROJECT_ID="31637767"
PACKAGE_NAME="my_package"
PACKAGE_VERSION="0.0.1"
FILE_NAME="hey.txt"
echo "https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${PACKAGE_VERSION}/${FILE_NAME}?status=default"
curl -X PUT --header "PRIVATE-TOKEN:${GITLAB_TOKEN_ADMIN}" \
     --upload-file ./hey.txt \
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${PACKAGE_VERSION}/${FILE_NAME}?status=default"
```
